## Object

Work and show data related to Coronavirus in Comunidad Valenciana (Spain)

## Based on
https://github.com/DavidHernandez/vlcCovidCharts
https://davidhernandez.github.io/vlcCovidCharts/index.html

## Demo
https://ja_ma.gitlab.io/coronavirus/

## Source

https://valenciacovid19-valenciacovid19.hub.arcgis.com/
