let day = 1;
let data = undefined;
let fields = undefined;
let buttonBefore = undefined;
let buttonAfter = undefined;

const chartColors = {
	$color1: 'rgba(215, 26, 25, 1)',
	$color2: 'rgba(116, 121, 128, 1)',
	$color3: 'rgba(172, 196, 211, 1)',
	$color4: 'rgba(240, 195, 162, 1)',
	$color5: 'rgba(212, 106, 51, 1)',
	$color6: 'rgba(216, 136, 101, 1)',
	$color7: 'rgba(99, 62, 56, 1)',
	$color1a: 'rgba(215, 26, 25, 1)',
	$color2a: 'rgba(116, 121, 128, 1)',
	$color3a: 'rgba(172, 196, 211, 1)',
	$color4a: 'rgba(240, 195, 162, 1)',
	$color5a: 'rgba(212, 106, 51, 1)',
	$color6a: 'rgba(216, 136, 101, 1)',
	$color7a: 'rgba(99, 62, 56, 1)',
}

const CHARACTERS = {
	PERMILLE: '&#8240',
	SQUARE: '&#9633;',
	TRIANGLE_UP: '&#9650;',
	TRIANGLE_DOWN: '&#9660;',
}

const POPULATION2019 = {
	VALENCIA: 2565124,
}

function initialize() {

	setListeners();
	buttonBefore = document.getElementById("minus");
	buttonAfter = document.getElementById("plus");

	const url = "https://geoportal.valencia.es/arcgis/rest/services/OCI/Covid19_Map/MapServer/1/query?f=json&where=(provincia%3D%27Castellon%27%20OR%20provincia%3D%27Castell%C3%B3%27%20OR%20provincia%3D%27Valencia%27%20OR%20provincia%3D%27Alicante%27%20OR%20provincia%3D%27Alacant%27%20OR%20provincia%3D%27València%27)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=fecha%20asc&resultOffset=0&resultRecordCount=10000";

	fetch(url)
	.then(function(response) {
		return response.json();
	})
	.then(function(myJson) {
		console.log(myJson);
		fields = ["casos", "fallecidos", "altas", "casos_activos", "hospitalizados", "uci", "casos_inc", "fallecidos_inc", "altas_inc", "casos_activos_inc", "hospitalizados_inc", "uci_inc"];

		let valenciaData = myJson.features.filter(item => item.attributes.provincia === "Valencia" || item.attributes.provincia === "València");
		valenciaData.map((item, index, array) => {
			item.attributes["casos_inc"] = array[index-1] ? (item.attributes["casos"] - array[index-1]["attributes"]["casos"]) : item.attributes["casos"];
			item.attributes["fallecidos_inc"] = array[index-1] ? (item.attributes["fallecidos"] - array[index-1]["attributes"]["fallecidos"]) : item.attributes["fallecidos"];
			item.attributes["altas_inc"] = array[index-1] ? (item.attributes["altas"] - array[index-1]["attributes"]["altas"]) : item.attributes["altas"];
			item.attributes["casos_activos"] = item.attributes["casos"] - item.attributes["fallecidos"] - item.attributes["altas"];
			item.attributes["casos_activos_inc"] = array[index-1] ? (item.attributes["casos_activos"] - array[index-1]["attributes"]["casos_activos"]) : item.attributes["casos_activos"];
			item.attributes["hospitalizados_inc"] = array[index-1] ? (item.attributes["hospitalizados"] - array[index-1]["attributes"]["hospitalizados"]) : item.attributes["hospitalizados"];
			item.attributes["uci_inc"] = array[index-1] ? (item.attributes["uci"] - array[index-1]["attributes"]["uci"]) : item.attributes["uci"];
		});

		data = valenciaData;
		// show(valenciaData);

		show(data);

		let preparedData = fields.map(field => prepareDataForChart(data, field))
		let chartLabels = data.map(item => item.attributes).map(attribute => formatDate(attribute.fecha));

		insertChart(chartLabels, preparedData);
	});

}

function setListeners(){
	document.getElementById("buttons").addEventListener("click", (e)=>{
		if(e.target.tagName == "INPUT"){
			if (e.target.id == "minus"){
				++day;
				day = (day >= data.length) ? data.length : day
			} else {
				--day;
				day = (day <= 1) ? 1 : day
			}
			show(data, day);
		}
	})
}

function show(data, day = 1){
	let lastDayData = data[data.length-day].attributes;
	let beforeLastDayData = (data.length-day >= 1) ? data[data.length-day-1].attributes : 0;
	let afterLastDayData = (day > 1) ? data[data.length-day+1].attributes : 0;

	// console.table(lastDayData);
	// console.table(beforeLastDayData);

	if(beforeLastDayData) buttonBefore.value = formatDate( beforeLastDayData.fecha );
	if(afterLastDayData) buttonAfter.value = formatDate( afterLastDayData.fecha );

	let result = "<h2>Últimos datos Valencia " + formatDate(lastDayData.fecha) + "</h2>";

	fields.map((field)=>{
		if(field.indexOf("_inc") == -1){
			result += formatData(lastDayData, beforeLastDayData, field);
		}
	});

	document.getElementById("result").innerHTML = result;
}

function formatData(lastDayData, beforeLastDayData, field){

	let casos_activos = lastDayData["casos"]-lastDayData["fallecidos"]-lastDayData["altas"];
	let diff = lastDayData[field] - beforeLastDayData[field] || 0;
	if (diff > 0) {
		className = "up";
		if(field == "altas") className = "down";
		character = CHARACTERS.TRIANGLE_UP;
	} else if (diff < 0) {
		className = "down";
		character = CHARACTERS.TRIANGLE_DOWN;

	} else {
		className = "";
		character = "=";
	}

	let diffHTML = `
	<span class='diff-value'>${Math.abs(diff)}</span>
	<span class="${className}">${character}</span>
	`;
	let proportion = "";
	if(field == "casos"){
		proportion = `<span class="proportion">${(Math.abs(lastDayData[field])/POPULATION2019.VALENCIA*100).toFixed(4)} %<span class="over"> sobre ${numberWithDot(POPULATION2019.VALENCIA)}</span> hab. </span>`;
	} else if(field=="fallecidos"){
		proportion = `<span class="proportion" title="Cálculo de aviones a razón de 220 pasajeros (Airbus A320)">${(Math.abs(lastDayData[field])/lastDayData["casos"]*100).toFixed(2)} % (${(lastDayData["fallecidos"]/220).toFixed(0)} ✈) <span class="over"> sobre ${numberWithDot(lastDayData["casos"])}</span> casos</span>`;
	} else if(field=="altas"){
		proportion = `<span class="proportion">${(Math.abs(lastDayData[field])/lastDayData["casos"]*100).toFixed(2)} %<span class="over"> sobre ${numberWithDot(lastDayData["casos"])}</span> casos</span>`;
	} else if(field=="hospitalizados"){
		proportion = `<span class="proportion">${(Math.abs(lastDayData[field])/casos_activos*100).toFixed(2)} %<span class="over"> sobre ${numberWithDot(casos_activos)} casos</span> activos</span>`;
	} else if(field=="casos_activos"){
		proportion = ``;//`<span class="proportion">${(Math.abs(lastDayData[field])/lastDayData["hospitalizados"]*100||0).toFixed(2)} %<span class="over"> sobre ${numberWithDot(lastDayData["hospitalizados"])}</span> hospit.</span>`;
	} else {
		proportion = `<span class="proportion">${(Math.abs(lastDayData[field])/lastDayData["hospitalizados"]*100||0).toFixed(2)} %<span class="over"> sobre ${numberWithDot(lastDayData["hospitalizados"])}</span> hospit.</span>`;
	}

	return `
	<div id="row">
	<span class='dataLabel'>${field.toUpperCase()}:</span>
	<span class='diff'>${diffHTML}</span>
	<span class='data'> ${numberWithDot(lastDayData[field])}</span>
	${proportion}
	</div>
	`;

}

function formatDate(dateReceived) {
	let date = new Date(dateReceived);

	return formatNumberWithZeros(date.getDate()) + "/" + formatNumberWithZeros((date.getMonth() + 1)) + "/" + date.getFullYear();
}

function formatNumberWithZeros(number){
	return number < 10 ? "0"+number : number;
}

function numberWithDot(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function prepareDataForChart(data, field){
	let chartData = data.map(item => item.attributes).map(attribute => attribute[field]);
	let preparedData = {
		label: field.toUpperCase(),
		data: chartData,
		fill: false,
		borderColor: chartColors[Object.keys(chartColors)[fields.indexOf(field)]],
		backgroundColor: chartColors[Object.keys(chartColors)[fields.indexOf(field)]],
	}
	if (field.endsWith("_inc")){
		debugger;
		preparedData.type = "bar";
	}

	return preparedData;
}

function insertChart(labels, datasets){
	var ctx = document.getElementById('myChart').getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: labels,
			datasets: datasets,
		},
		options: {
			legend: {
            display: true,
            labels: {
                fontSize: 10,
            }
        },
			responsive: true,
			maintainAspectRatio: false,
			title: {
				display: true,
				text: 'Coronavirus en Provincia de Valencia'
			},
			tooltips: {
				position: 'nearest',
				mode: 'index',
				intersect: false,
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Fecha'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Número'
					}
				}]
			},
			plugins: {
				zoom: {
					pan: {
						enabled: true,
						mode: 'x',
						sensitivity: 3,
					},
					zoom: {
						enabled: true,
						mode: 'x',
						sensitivity: 3,
					}
				}
			}
		},
	});
}
